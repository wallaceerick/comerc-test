var App = {

	init: function(){
		this.menu();
		this.fade();
		this.mansonry();
		this.infinite();

		$(window).scroll(function(){
			
			var header     = $('.js-header'),
				size       = header.height(),
				scrollTop  = window.pageYOffset;

			if(scrollTop >= (size / 2)){
				header.addClass('fixed');
			} 
			else {
				header.removeClass('fixed');
			}
			
        });

	},

	menu: function(){
		var button = $('.js-open-menu');

		button.click(function() {
			$(this).toggleClass('active');
		});
	},

	fade: function(){	

		var i, 
		itens = $('.js-animated-block');

		for(i = 0; i < itens.length; i++){
		
			(function(i){
				setTimeout(function(){
					itens[i].style.opacity = 1;
				}, 300 * i)
			})(i);
			
		}
	
	}, 
	
	mansonry: function(){
		var $container = $('.js-masonry'),
			item       = '.categories__item';

		$container.imagesLoaded(function(){
			$container.masonry({
				itemSelector:    item,
				columnWidth:     10,
				percentPosition: true
			});
		});

	},

	infinite: function(){

		var $container = $('.js-infinite-scroll'),
			item       = '.categories__item';

		$container.infinitescroll({
			navSelector:  '.js-scroll-navigation',
			nextSelector: '.js-scroll-navigation a',
			itemSelector: item,
			loading: {
				msgText:     'Page Loader',
				finishedMsg: 'No more pages to load',
				img:         'assets/images/loading.gif'
			}
		}, function(newElements) {
			var $newElems = $(newElements).css({
				opacity: 0
			});
			
			$newElems.imagesLoaded(function(){
				$newElems.animate({
					opacity: 1
				});
				$container.masonry('appended', $newElems, true); 
			});

		});
	}

};

App.init();