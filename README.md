# Comerc
http://www.wallaceerick.com.br/clientes/testes/comerc/

## Requerimentos
O gerenciador de tarefas utilizado neste projeto é o `Gulp` e o pré-processador de CSS o `Sass`, você precisa ter instalado:

1. [Ruby 2.x](https://rubyinstaller.org/)
2. [Node 3.x](https://nodejs.org/en/download/)
3. [Gulp CLI](https://gulpjs.com/)

Caso já tenha todos, instale as dependências e inicie o servidor.

1. `npm install` para instalar todas as dependências do projeto.
2. `gulp server` para iniciar o servidor e abrir a página `http://localhost:3000`.

## Documentação

### JS
As bibliotecas utilizadas no projeto ficam na pasta **assets/js/libraries**, para este projeto estamos utilizando o jQuery. Os plugins do jQuery ficam na pasta **assets/js/plugins**, não são alterados e nem configurados aqui, isso será feito pelo arquivos da pasta **assets/js/source**, onde ficam todos os arquivos de chamadas dos plugins e funcionalidades/métodos utlizados no projeto.

Os seletores dos elementos que terão ações com JS, devem ser declarados e cacheados no início do arquivo. Todos os seletores devem conter o prefixo 'js' na classe, para separar todas as interações do JS das classes de estilização do CSS.

```javascript
var infiniteScroll  = $('.js-infinte-scroll'),
    masonryGrid     = $('.js-masonry');
```

### CSS
Estamos seguindo uma arquitetura chamada [ITCSS](https://www.xfive.co/blog/itcss-scalable-maintainable-css-architecture) (Triangle Inverted CSS), combinada com o [BEM](http://getbem.com/introduction/).
Classes e IDs devem ser escritos em inglês. Para as classes, separar as palavras por hífen e manter os caracteres em lowercase **.page-content**. Já os IDs, devem ser escrito em Capitalize, sem separação de palavras **#pageContent**, caso as classes e IDs sejam uma única palavra, manter o padrão em lowercase **#content**.

**Estrutura de Arquivos**

* **settings:** Variáveis globais, configurações e definições de cor.
* **tools:** Bibliotecas, mixins e funcões gerais.
* **generic:** Ajuste nas propriedades dos elementos, sem estilos de cor e formatação.
* **elements:** Estilos para elementos HTML que não utilizam classes.
* **objects:** Estilos baseados em classes para elementos específicos, sem decoraão visual.
* **components:** Todos os componentes utilizados no projetos, estilizados com classes.
* **trumps:** Utilidades e helpers com o poder de sobrescrever propriedades e valores de elementos.
